import type { Config } from "drizzle-kit";

export default {
    schema: "./src/app/db/schema.ts",
    out: "./drizzle",
    driver:"pg",
    dbCredentials: {
        connectionString: "postgresql://postgres:root@localhost:5432/react_trpc"
    }
    
} satisfies Config;