import { pgTable, serial, text, varchar, integer } from "drizzle-orm/pg-core";
 
export const todos = pgTable('todos', {
  id: serial('id').primaryKey(),
  content: text('content'),
  done: integer('done'),
});