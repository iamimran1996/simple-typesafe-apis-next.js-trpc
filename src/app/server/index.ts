import { eq } from "drizzle-orm";
import { router, publicProcedure } from "./trpc";
import { Client } from "pg";
import { drizzle } from "drizzle-orm/node-postgres";

import { todos } from "../db/schema";

const pg = new Client({
    connectionString: "postgresql://postgres:root@localhost:5432/react_trpc"
});
await pg.connect();
const db = drizzle(pg);


export const appRouter = router({
    getTodos: publicProcedure.query(async () => {
        const res = await db.select().from(todos);
        return res;
    }),
    addTodo: publicProcedure.mutation(async (opts) => {
        await db.insert(todos).values({ content: opts.rawInput, done: 0 }).returning();
        return true;
    }),
    setDone: publicProcedure.mutation(async (opts) => {
        await db.update(todos).set({
            done: opts.rawInput.done,
        }).where(eq(todos.id, opts.rawInput.id)).returning();
        return true;
    })
});

export type AppRouter = typeof appRouter;