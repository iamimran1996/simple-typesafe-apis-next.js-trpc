import styles from './page.module.css';
import TodoList from './_components/TodoList';

import { serverClient } from './_trpc/serverClient';

export default async function Home() {
  const getTodos = await serverClient.getTodos()
  return (
    <main className={styles.main}>
        <TodoList initialTodos={getTodos}></TodoList>
    </main>
  )
}
